package com.music.list.service.impl;

import com.music.list.mapper.UserMapper;
import com.music.list.repository.AuthRepo;
import com.music.list.repository.UserRepository;
import com.music.list.security.Authority;
import com.music.list.security.User;
import com.music.list.security.UserDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {
    @Mock
    UserRepository userRepository;
    @Mock
    UserMapper userMapper;
    @Mock
    PasswordEncoder passwordEncoder;
    @Mock
    AuthRepo authRepo;

    @InjectMocks
    UserServiceImpl userService;

    User user;
    UserDto userDto;
    Authority authority;

    @BeforeEach
    void setUp() {
        authority=Authority.builder()
                .id(2L)
                .authority("ROLE_USER")
                .build();
        user=User.builder()
                .id(1L)
                .email("test@mail.ru")
                .password(passwordEncoder.encode("test"))
                .name("test")
                .username("test")
                .authorities(Set.of(authority))
                .build();
        userDto=UserDto.builder()
                .email("test@mail.ru")
                .password(passwordEncoder.encode("test"))
                .name("test")
                .username("test")
                .build();
    }

    @Test
    void create() {
        when(authRepo.getById(2L)).thenReturn(authority);
        when(userRepository.existsByUsername(anyString())).thenReturn(false);
        when(userMapper.user(userDto)).thenReturn(user);
        when(userRepository.save(user)).thenReturn(user);

        userService.create(userDto);

        assertThat(userDto.getUsername()).isEqualTo("test");
    }


}