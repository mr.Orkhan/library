package com.music.list.service.impl;

import com.music.list.domain.Singer;
import com.music.list.dto.SingerDto;
import com.music.list.mapper.SingerMapper;
import com.music.list.repository.SingerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class SingerServiceImplTest {
    @Mock
    SingerRepository singerRepository;
    @Mock
    SingerMapper singerMapper;
    @InjectMocks
    SingerServiceImpl singerService;

    Singer singer;

    SingerDto singerDto;
    @BeforeEach
    void setUp(){
        singer=Singer.builder()
                .id(1L)
                .name("Shovkat")
                .build();

        singerDto=SingerDto.builder()
                .name("Shovkat")
                .build();

    }

    @Test
    void edit() {
        when(singerRepository.findById(anyLong())).thenReturn(Optional.of(singer));
        when(singerMapper.singerDto(singer)).thenReturn(singerDto);

        SingerDto singerDto1=singerService.edit(1L,singerDto);

        assertThat(singerDto1.getName()).isEqualTo("Shovkat");

        verify(singerRepository,times(1)).findById(anyLong());
        verify(singerMapper,times(1)).singerDto(singer);
    }

    @Test
    void delete() {
        when(singerRepository.findById(anyLong())).thenReturn(Optional.of(singer));
        doNothing().when(singerRepository).delete(singer);
        singerService.delete(1L);

        verify(singerRepository,times(1)).findById(anyLong());
        verify(singerRepository,times(1)).delete(singer);
    }

}