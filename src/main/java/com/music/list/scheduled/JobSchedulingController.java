package com.music.list.scheduled;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/schedule")
@RequiredArgsConstructor
public class JobSchedulingController {


    private final Scheduled taskSchedulingService;



    @PostMapping(path="/taskdef")
    public void scheduleATask(@RequestParam String taskDefinition) {

        taskSchedulingService.scheduleATask("one", taskSchedulingService, taskDefinition);
    }


}
