package com.music.list.mapper;

import com.music.list.domain.Singer;
import com.music.list.domain.Song;
import com.music.list.dto.SongDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface SongMapper {
    SongDto songDto(Song song);
    Song song(SongDto songDto);

    List<SongDto> songDtoList(List<Song> songList);
    List<Song> songList(List<SongDto> songDtoList);

}
