package com.music.list.mapper;

import com.music.list.security.User;
import com.music.list.security.UserDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface UserMapper {

    User user(UserDto userDto);
    UserDto userDto(User user);
}
