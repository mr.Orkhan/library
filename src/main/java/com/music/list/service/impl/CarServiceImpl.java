package com.music.list.service.impl;


import com.music.list.client.GroupClient;
import com.music.list.dto.CarDto;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class CarServiceImpl {

    GroupClient examClient;

    public List<CarDto> findAll(){
        return examClient.findAll();
    }
}
