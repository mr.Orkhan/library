package com.music.list.service.impl;

import com.music.list.domain.Singer;
import com.music.list.dto.SingerDto;
import com.music.list.dto.SongDto;
import com.music.list.mapper.SingerMapper;
import com.music.list.page.SingFilter;
import com.music.list.repository.SingerRepository;
import com.music.list.service.SingerService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import javax.transaction.Status;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class SingerServiceImpl implements SingerService {


    SingerRepository singerRepository;
    SingerMapper singerMapper;

    @Override
    public void create(SingerDto singerDto) {
        singerRepository.save(singerMapper.singer(singerDto));
    }

    @Override
    @Transactional
    public SingerDto edit(Long id, SingerDto singerDto) {
        Singer singer=singerRepository.findById(id)
                .orElseThrow(()->new RuntimeException("not found"));

        singer.setName(singerDto.getName());

        return singerMapper.singerDto(singer);

    }

    @Override
    public void delete(Long id) {
        Singer singer=singerRepository.findById(id)
                .orElseThrow(()->new RuntimeException("not found"));

        singerRepository.delete(singer);
    }

    @Override
    public Page<SongDto> findAll() {

        return null;
    }

    @Override
    public Page<SingerDto> findAllSinger(SingFilter filter, Pageable pageable) {
        Page<Singer> singers = singerRepository.findAll((Specification<Singer>) (root, query, criteriaBuilder) -> {
            Predicate predicate = criteriaBuilder.conjunction();
            if (filter.getName() != null) {
                predicate = criteriaBuilder.and(predicate, criteriaBuilder.like(root.get("name"), "%" + filter.getName().toLowerCase() + "%"));
            }
          return predicate;
        },pageable);
        return singers.map(singerMapper::singerDto);
    }
}
