package com.music.list.service.impl;

import com.music.list.constant.Role;
import com.music.list.mapper.UserMapper;
import com.music.list.repository.AuthRepo;
import com.music.list.repository.UserRepository;
import com.music.list.security.Authority;
import com.music.list.security.User;
import com.music.list.security.UserDto;
import com.music.list.service.UserService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class UserServiceImpl implements UserService {

    UserRepository userRepository;
    UserMapper userMapper;
    PasswordEncoder passwordEncoder;

    AuthRepo authRepo;


    @Override
    @Transactional
    public void create(UserDto userDto) {

        Authority authority=authRepo.getById(2L);

        if (userRepository.existsByUsername(userDto.getUsername())){
            throw new RuntimeException("artiq movcuddur");
        }

        User user=userMapper.user(userDto);
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        user.setAuthorities(Set.of(authority));

        userRepository.save(user);

    }

    @Override
    @Transactional
    public void changeRole(Long id, Long roleId) {
        User user=userRepository.findById(id).orElseThrow();
        Authority authority=authRepo.findById(roleId).orElseThrow();


       authority.setAuthority(authority.getAuthority());
       user.setAuthorities(Set.of(authority));


    }

    @Override
    public void add() {
        String role=Role.ROLE_ADMIN.toString();
        Authority authority=Authority.builder()
                .authority(role)
                .build();
        authRepo.save(authority);

    }



    @Override

    public List<User> findUser() {

        return null;
    }


}
