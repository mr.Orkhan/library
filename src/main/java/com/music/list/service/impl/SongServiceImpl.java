package com.music.list.service.impl;

import com.music.list.domain.Singer;
import com.music.list.domain.Song;
import com.music.list.dto.SongDto;
import com.music.list.mapper.SongMapper;
import com.music.list.page.SongFilter;
import com.music.list.repository.SingerRepository;
import com.music.list.repository.SongRepository;
import com.music.list.response.SongResponse;
import com.music.list.service.SongService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ScheduledFuture;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class SongServiceImpl implements SongService {
    SongRepository songRepository;

    SingerRepository singerRepository;
    SongMapper songMapper;



    private final TaskScheduler taskScheduler;

    Map<String, ScheduledFuture<?>> jobsMap = new HashMap<>();

    private void set(String jobId, Runnable tasklet, String cronExpression){
        System.out.println("Scheduling task with job id: " + jobId + " and cron expression: " + cronExpression);
        ScheduledFuture<?> scheduledTask = taskScheduler.schedule(tasklet, new CronTrigger(cronExpression, TimeZone.getTimeZone(TimeZone.getDefault().getID())));
        jobsMap.put(jobId, scheduledTask);
    }




    @Override
    public void create(SongDto songDto) {
        songRepository.save(songMapper.song(songDto));
    }

    @Override
    @Transactional

    public SongDto edit(Long id, SongDto songDto) {
        Song song=songRepository.findById(id).orElseThrow(()->new RuntimeException("not found"));
        song.setName(songDto.getName());
        return songMapper.songDto(song);
    }

    @Override
    public void delete(Long id) {
        Song song=songRepository.findById(id).orElseThrow(()->new RuntimeException("not found"));

        songRepository.delete(song);

    }

    @Override
    public Page<SongDto> findAll( SongFilter songFilter, Pageable pageable) {
        Page<Song> songs=songRepository.findAll( pageable);
        return songs.map(songMapper::songDto);
    }


    @Override
    public Page<SongDto> findSingerId(Long id, SongFilter songFilter, Pageable pageable) {
        Page<Song> songs=songRepository.findBySinger_Id(id, pageable);
        return songs.map(songMapper::songDto);
    }

    @Override
    @Transactional
    public void addSong(Long id, Long singerId) {
        Song song=songRepository.findById(id).orElseThrow(()->new RuntimeException("not found"));
        Singer singer=singerRepository.findById(singerId)
                .orElseThrow(()->new RuntimeException("not found"));

        song.setSinger(singer);

    }

    @Override
    public void scheduled() {

    }




    @Override
    public SongResponse music(Long id) {
        Singer singer=singerRepository.findById(id).orElseThrow();

       return SongResponse.builder()
                .singerName(singer.getName())
                .song(songMapper.songDtoList(singer.getSongList()))
                .build();

    }


}
