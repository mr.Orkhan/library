package com.music.list.jwttoken;

import com.music.list.repository.UserRepository;
import com.music.list.security.User;
import com.music.list.security.UserDto;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service

public class JwtService {
    private final Key key;
    private final UserRepository userRepository;

    public JwtService(UserRepository userRepository){
        byte[] keyBytes;
        String keyString = "dGhpcyBpcyB0aGFuIGV4YW1wbGV0aGlzIGlzIHRoYW4gZXhhbXBsZSB0aGlzIGlzIHRoYW4gZXhhbXBsZXRoaXMgaXMgdGhhbiBleGFtcGxl";
        keyBytes= Decoders.BASE64.decode(keyString);
        key= Keys.hmacShaKeyFor(keyBytes);
        this.userRepository=userRepository;
    }
    public String issueToken(UserDto authentication){
        User user=userRepository.findByUsername(authentication.getUsername()).orElseThrow();
        List<String> collect = user.getAuthorities()
                .stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());
        final JwtBuilder jwtBuilder = Jwts.builder()
                .setSubject(authentication.getUsername())
                .setIssuedAt(new Date())
                .setExpiration(Date.from(Instant.now().plus(
                        Duration.ofSeconds(300000))))
                .setHeader(Map.of("type", "JWT"))
                .addClaims(Map.of("role", collect))
                .signWith(key, SignatureAlgorithm.HS512);
        return jwtBuilder.compact();
    }



    public Claims parseToken(String token){
        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }


}
