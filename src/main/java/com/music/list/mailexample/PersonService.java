package com.music.list.mailexample;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class PersonService {

    private final JavaMailSender javaMailSender;
    public ResponseEntity<Object> sendEmail(Person person,String msg,String text){

        SimpleMailMessage sm=new SimpleMailMessage();
        sm.setFrom("orxansamed111@gmail.com");
        sm.setSubject(msg);
        sm.setText(text);
        sm.setTo(person.getEmail());
        javaMailSender.send(sm);
        return generated("Email Sent to the mail "+person.getEmail(),HttpStatus.OK,person);
    }

    private ResponseEntity<Object>  generated(String msg, HttpStatus hs, Object o){
        Map<String,Object> mp=new HashMap<>();
         mp.put("message",msg);
         mp.put("status",hs);
         mp.put("object",o);

        return new ResponseEntity<Object>(mp,hs);
    }

    public void  send(String to,String body , String topic){
        System.out.println("sending method");
        SimpleMailMessage sm=new SimpleMailMessage();
        sm.setFrom("orxansamed111@gmail.com");
        sm.setTo(to);
        sm.setSubject(topic);
        sm.setText(body);

        javaMailSender.send(sm);

        System.out.println("ending method");
    }
}
