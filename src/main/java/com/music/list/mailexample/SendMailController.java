package com.music.list.mailexample;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/send")
@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class SendMailController {

    PersonService personService;

    @PostMapping("/message")
    public ResponseEntity<Object> sendEmail(Person person,String msg,String text){
        return personService.sendEmail(person,msg,text);
    }

    @PostMapping("/send-messae")
    public void  send(@RequestParam String to,
                      @RequestParam String body ,
                      @RequestParam String topic){
        personService.send(to,body,topic);
    }
}
