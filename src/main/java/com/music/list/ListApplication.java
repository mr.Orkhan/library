package com.music.list;

import com.music.list.mailexample.PersonService;
import com.music.list.service.impl.UserServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@RequiredArgsConstructor
@EnableScheduling
@EnableFeignClients
public class ListApplication implements CommandLineRunner {
private final UserServiceImpl userService;
	public static void main(String[] args) {
		SpringApplication.run(ListApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
   // userService.add();
	}
}
