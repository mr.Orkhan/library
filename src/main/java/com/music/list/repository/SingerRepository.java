package com.music.list.repository;

import com.music.list.domain.Singer;
import com.music.list.domain.Song;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SingerRepository extends JpaRepository<Singer,Long>, JpaSpecificationExecutor<Singer> {

}
