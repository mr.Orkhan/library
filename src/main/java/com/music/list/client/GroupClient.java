package com.music.list.client;

import com.music.list.dto.CarDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(name = "exam",url = "http://localhost:8081/")
public interface GroupClient {


    @GetMapping("api/car/find-all")
    public List<CarDto> findAll();
}
