package com.music.list.controller;

import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;
import com.music.list.constant.Role;
import com.music.list.security.UserDto;
import com.music.list.service.impl.UserServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserController {
    private final UserServiceImpl userService;


    @PostMapping("/register")
    public void create(@RequestBody UserDto userDto) {
        userService.create(userDto);
    }

}
