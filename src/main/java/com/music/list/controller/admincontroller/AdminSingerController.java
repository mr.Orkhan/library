package com.music.list.controller.admincontroller;

import com.music.list.dto.SingerDto;
import com.music.list.service.impl.SingerServiceImpl;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/admin/singer")
@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class AdminSingerController {
   private final SingerServiceImpl singerService;

    @PostMapping("/save")

    public void create(@RequestBody SingerDto singerDto) {
        singerService.create(singerDto);

    }

    @PutMapping("/edit/id/{id}")
    public SingerDto edit(@PathVariable Long id,
                          @RequestBody SingerDto singerDto) {

        return singerService.edit(id,singerDto);
    }

    @DeleteMapping("/delete/id/{id}")
    public void delete(@PathVariable Long id) {

        singerService.delete(id);
    }

}
