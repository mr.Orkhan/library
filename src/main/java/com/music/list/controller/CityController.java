package com.music.list.controller;

import com.music.list.dto.CarDto;
import com.music.list.service.impl.CarServiceImpl;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/city")
@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class CityController {
    CarServiceImpl carService;

    @GetMapping("/find-all")
    public List<CarDto> findAll(){
        return carService.findAll();
    }
}
